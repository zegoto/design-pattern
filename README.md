# DesignPattern

#### 介绍
设计模式（23种设计模式 示例代码）, 帮助理解面向对象软件开发过程中使用的到设计模式思想

#### 工程目录

```txt
├── main
│   ├── java
│   │   └── com
│   │       └── zegoto
│   │           └── design
│   │               ├── controller
│   │               ├── design
│   │               │   ├── behavior
│   │               │   │   ├── chain
│   │               │   │   ├── command
│   │               │   │   ├── interpreter
│   │               │   │   ├── iterator
│   │               │   │   ├── mediator
│   │               │   │   ├── memento
│   │               │   │   ├── observer
│   │               │   │   ├── state
│   │               │   │   ├── strategy
│   │               │   │   ├── template
│   │               │   │   └── visitor
│   │               │   ├── create
│   │               │   │   ├── builder
│   │               │   │   ├── factory
│   │               │   │   │   ├── AbstractFactory
│   │               │   │   │   ├── SimpleFactory
│   │               │   │   │   └── factoryMethod
│   │               │   │   ├── prototype
│   │               │   │   └── singleton
│   │               │   └── struct
│   │               │       ├── adapter
│   │               │       ├── appearance
│   │               │       ├── bridge
│   │               │       ├── composite
│   │               │       ├── decorator
│   │               │       ├── flyweight
│   │               │       └── proxy
│   │               ├── model
│   │               └── service
│   └── resources
│       ├── static
│       └── templates
└── test
```


