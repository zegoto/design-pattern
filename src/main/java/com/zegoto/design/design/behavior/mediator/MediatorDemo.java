package com.zegoto.design.design.behavior.mediator;


// 中介者接口
interface ChatRoom {
    void sendMessage(String message, User user);
}

// 具体的中介者类
class ChatRoomImpl implements ChatRoom {
    public void sendMessage(String message, User user) {
        System.out.println(user.getName() + " sends message: " + message);
    }
}

// 用户类
class User {
    private String name;
    private ChatRoom chatRoom;

    public User(String name, ChatRoom chatRoom) {
        this.name = name;
        this.chatRoom = chatRoom;
    }

    public String getName() {
        return name;
    }

    public void sendMessage(String message) {
        chatRoom.sendMessage(message, this);
    }
}

public class MediatorDemo {

    public static void main(String[] args) {
        ChatRoom chatRoom = new ChatRoomImpl();

        User alice = new User("Alice", chatRoom);
        User bob = new User("Bob", chatRoom);
        User charlie = new User("Charlie", chatRoom);

        alice.sendMessage("Hello Bob!");
        bob.sendMessage("Hi Alice!");
        charlie.sendMessage("Hi everyone!");
    }

}
