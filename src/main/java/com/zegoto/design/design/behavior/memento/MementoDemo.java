package com.zegoto.design.design.behavior.memento;



// 备忘录类，用于保存编辑器的状态
class EditorState {
    private final String content;

    public EditorState(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
}

// 编辑器类，用于创建和恢复备忘录对象
class Editor {
    private String content;

    public void setContent(String content) {
        this.content = content;
    }

    public EditorState createState() {
        return new EditorState(content);
    }

    public void restoreState(EditorState state) {
        content = state.getContent();
    }

    public String getContent() {
        return content;
    }
}

public class MementoDemo {

    public static void main(String[] args) {
        Editor editor = new Editor();

        // 编辑器输入文本
        editor.setContent("This is the first sentence.");
        System.out.println(editor.getContent());

        // 创建备忘录对象并保存状态
        EditorState state = editor.createState();

        // 编辑器继续输入文本
        editor.setContent("This is the second sentence.");
        System.out.println(editor.getContent());

        // 恢复之前的状态
        editor.restoreState(state);
        System.out.println(editor.getContent());
    }

}
