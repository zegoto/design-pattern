package com.zegoto.design.design.behavior.chain;

// 抽象处理器类
abstract class Handler {
    protected Handler successor;

    public void setSuccessor(Handler successor) {
        this.successor = successor;
    }

    public abstract void handleRequest(LeaveRequest request);
}

// 具体处理器类
class Manager extends Handler {
    public void handleRequest(LeaveRequest request) {
        if (request.getLeaveDays() <= 2) {
            System.out.println("Leave request approved by Manager.");
        } else if (successor != null) {
            successor.handleRequest(request);
        }
    }
}

class DepartmentHead extends Handler {
    public void handleRequest(LeaveRequest request) {
        if (request.getLeaveDays() > 2 && request.getLeaveDays() <= 5) {
            System.out.println("Leave request approved by Department Head.");
        } else if (successor != null) {
            successor.handleRequest(request);
        }
    }
}

class CEO extends Handler {
    public void handleRequest(LeaveRequest request) {
        if (request.getLeaveDays() > 5) {
            System.out.println("Leave request approved by CEO.");
        } else if (successor != null) {
            successor.handleRequest(request);
        }
    }
}

// 请假请求类
class LeaveRequest {
    private String name;
    private int leaveDays;

    public LeaveRequest(String name, int leaveDays) {
        this.name = name;
        this.leaveDays = leaveDays;
    }

    public String getName() {
        return name;
    }

    public int getLeaveDays() {
        return leaveDays;
    }
}

public class ChainDemo {

    public static void main(String[] args) {
        Handler manager = new Manager();
        Handler departmentHead = new DepartmentHead();
        Handler ceo = new CEO();

        manager.setSuccessor(departmentHead);
        departmentHead.setSuccessor(ceo);

        LeaveRequest request1 = new LeaveRequest("John", 1);
        manager.handleRequest(request1);

        LeaveRequest request2 = new LeaveRequest("Mary", 4);
        manager.handleRequest(request2);

        LeaveRequest request3 = new LeaveRequest("Tom", 10);
        manager.handleRequest(request3);
    }

}
