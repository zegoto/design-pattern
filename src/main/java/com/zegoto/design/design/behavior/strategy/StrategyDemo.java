package com.zegoto.design.design.behavior.strategy;


// Strategy 接口
interface CalculatorStrategy {
    int calculate(int a, int b);
}

// ConcreteStrategy 实现加法
class AddStrategy implements CalculatorStrategy {
    public int calculate(int a, int b) {
        return a + b;
    }
}

// ConcreteStrategy 实现减法
class SubStrategy implements CalculatorStrategy {
    public int calculate(int a, int b) {
        return a - b;
    }
}

// Context 类
class CalculatorContext {
    private CalculatorStrategy strategy;

    public CalculatorContext(CalculatorStrategy strategy) {
        this.strategy = strategy;
    }

    public void setStrategy(CalculatorStrategy strategy) {
        this.strategy = strategy;
    }

    public int calculate(int a, int b) {
        return strategy.calculate(a, b);
    }
}

// 客户端代码
public class StrategyDemo {
    public static void main(String[] args) {
        CalculatorContext calculator = new CalculatorContext(new AddStrategy());
        System.out.println("1 + 2 = " + calculator.calculate(1, 2));

        calculator.setStrategy(new SubStrategy());
        System.out.println("2 - 1 = " + calculator.calculate(2, 1));
    }
}
