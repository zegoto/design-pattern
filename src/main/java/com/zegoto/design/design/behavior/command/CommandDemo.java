package com.zegoto.design.design.behavior.command;


// 定义命令接口
interface Command {
    void execute();
}

// 具体的命令实现类
class LightOnCommand implements Command {
    private Light light;

    public LightOnCommand(Light light) {
        this.light = light;
    }

    public void execute() {
        light.switchOn();
    }
}

class LightOffCommand implements Command {
    private Light light;

    public LightOffCommand(Light light) {
        this.light = light;
    }

    public void execute() {
        light.switchOff();
    }
}

// 接收者类
class Light {
    public void switchOn() {
        System.out.println("灯被打开了！");
    }

    public void switchOff() {
        System.out.println("灯被关闭了！");
    }
}

// 调用者类
class RemoteControl {
    private Command command;

    public void setCommand(Command command) {
        this.command = command;
    }

    public void pressButton() {
        command.execute();
    }
}


public class CommandDemo {

    public static void main(String[] args) {
        // 创建接收者对象
        Light light = new Light();

        // 创建命令对象
        LightOnCommand lightOnCommand = new LightOnCommand(light);
        LightOffCommand lightOffCommand = new LightOffCommand(light);

        // 创建调用者对象
        RemoteControl remoteControl = new RemoteControl();

        // 设置命令对象
        remoteControl.setCommand(lightOnCommand);
        remoteControl.pressButton();

        remoteControl.setCommand(lightOffCommand);
        remoteControl.pressButton();
    }

}
