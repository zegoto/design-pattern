中介者设计模式是一种行为型设计模式，它允许对象之间通过中介者对象进行通信，从而减少对象之间的耦合度。该模式将对象之间的交互逻辑封装到中介者对象中，并且使得对象只与中介者对象进行通信，而不直接与其他对象进行通信。中介者模式通常用于处理多个对象之间的复杂交互，例如聊天室或者飞机调度等。

我们定义了一个ChatRoom接口作为中介者，并实现了ChatRoomImpl类作为具体的中介者。我们还定义了一个User类作为聊天室中的用户，并在其中包含了中介者对象。

在客户端代码中，我们创建了一个ChatRoomImpl对象作为中介者，并创建了三个User对象，并将中介者对象传递给它们。然后，我们依次调用了三个用户对象的sendMessage(String message)方法，通过中介者对象进行通信。

通过使用中介者设计模式，我们可以将多个对象之间的交互逻辑封装到中介者对象中，并使得对象只与中介者对象进行通信，从而减少对象之间的耦合度。此外，中介者模式还可以帮助我们简化对象之间的交互逻辑，从而使得代码更加清晰和易于理解。


