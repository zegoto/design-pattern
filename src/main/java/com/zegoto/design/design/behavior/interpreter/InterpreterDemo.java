package com.zegoto.design.design.behavior.interpreter;


import java.util.Stack;

// 抽象表达式类
interface Expression {
    int interpret();
}

// 具体表达式类
class NumberExpression implements Expression {
    private int number;

    public NumberExpression(int number) {
        this.number = number;
    }

    public int interpret() {
        return number;
    }
}

class AddExpression implements Expression {
    private Expression leftExpression;
    private Expression rightExpression;

    public AddExpression(Expression leftExpression, Expression rightExpression) {
        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
    }

    public int interpret() {
        return leftExpression.interpret() + rightExpression.interpret();
    }
}

class SubtractExpression implements Expression {
    private Expression leftExpression;
    private Expression rightExpression;

    public SubtractExpression(Expression leftExpression, Expression rightExpression) {
        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
    }

    public int interpret() {
        return leftExpression.interpret() - rightExpression.interpret();
    }
}

// 上下文类
class Context {
    private String input;
    private int output;

    public Context(String input) {
        this.input = input;
    }

    public String getInput() {
        return input;
    }

    public void setOutput(int output) {
        this.output = output;
    }

    public int getOutput() {
        return output;
    }
}

// 解释器类
class Interpreter {
    private Expression expression;

    public Interpreter(Expression expression) {
        this.expression = expression;
    }

    public int interpret(Context context) {
        int result = expression.interpret();
        context.setOutput(result);
        return result;
    }
}


public class InterpreterDemo {

    public static void main(String[] args) {
        String input = "5 + 4 - 3 + 2 - 1";
        Context context = new Context(input);

        String[] tokens = input.split(" ");
        Stack<Expression> stack = new Stack<>();

        for (int i = 0; i < tokens.length; i++) {
            if (tokens[i].equals("+")) {
                Expression leftExpression = stack.pop();
                Expression rightExpression = new NumberExpression(Integer.parseInt(tokens[++i]));
                stack.push(new AddExpression(leftExpression, rightExpression));
            } else if (tokens[i].equals("-")) {
                Expression leftExpression = stack.pop();
                Expression rightExpression = new NumberExpression(Integer.parseInt(tokens[++i]));
                stack.push(new SubtractExpression(leftExpression, rightExpression));
            } else {
                stack.push(new NumberExpression(Integer.parseInt(tokens[i])));
            }
        }

        Interpreter interpreter = new Interpreter(stack.pop());
        int result = interpreter.interpret(context);

        System.out.println("Input: " + context.getInput());
        System.out.println("Output: " + context.getOutput());
    }

}
