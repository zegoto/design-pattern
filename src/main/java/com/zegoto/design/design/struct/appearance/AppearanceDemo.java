package com.zegoto.design.design.struct.appearance;


// 画面类
class Screen {
    public void turnOn() {
        System.out.println("Screen is turned on");
    }

    public void turnOff() {
        System.out.println("Screen is turned off");
    }
}

// 音响类
class Audio {
    public void turnOn() {
        System.out.println("Audio is turned on");
    }

    public void turnOff() {
        System.out.println("Audio is turned off");
    }
}

// 遥控器类
class RemoteControl {
    private Screen screen;
    private Audio audio;

    public RemoteControl(Screen screen, Audio audio) {
        this.screen = screen;
        this.audio = audio;
    }

    public void turnOn() {
        System.out.println("Remote control is turned on");
    }

    public void turnOff() {
        System.out.println("Remote control is turned off");
    }
}

class TV {
    private Screen screen;
    private Audio audio;
    private RemoteControl remoteControl;

    public TV() {
        this.screen = new Screen();
        this.audio = new Audio();
        this.remoteControl = new RemoteControl(screen, audio);
    }

    public void turnOn() {
        screen.turnOn();
        audio.turnOn();
        remoteControl.turnOn();
    }

    public void turnOff() {
        screen.turnOff();
        audio.turnOff();
        remoteControl.turnOff();
    }
}


public class AppearanceDemo {

    // 客户端使用外观类来操作电视机
    public static void main(String[] args) {
        TV tv = new TV();
        tv.turnOn();
        tv.turnOff();
    }

}
