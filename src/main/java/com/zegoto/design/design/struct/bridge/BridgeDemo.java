package com.zegoto.design.design.struct.bridge;


// 桥接接口
interface GraphicsImpl {
    void drawCircle(int x, int y, int radius);
    void drawRectangle(int x1, int y1, int x2, int y2);
}

// Windows 平台实现
class WindowsGraphicsImpl implements GraphicsImpl {
    public void drawCircle(int x, int y, int radius) {
        System.out.println("Drawing circle on Windows platform");
    }

    public void drawRectangle(int x1, int y1, int x2, int y2) {
        System.out.println("Drawing rectangle on Windows platform");
    }
}

// Linux 平台实现
class LinuxGraphicsImpl implements GraphicsImpl {
    public void drawCircle(int x, int y, int radius) {
        System.out.println("Drawing circle on Linux platform");
    }

    public void drawRectangle(int x1, int y1, int x2, int y2) {
        System.out.println("Drawing rectangle on Linux platform");
    }
}

// 抽象图形类
abstract class Graphics {
    protected GraphicsImpl impl;

    public Graphics(GraphicsImpl impl) {
        this.impl = impl;
    }

    public abstract void draw();
}

// 圆形类
class Circle extends Graphics {
    private int x, y, radius;

    public Circle(GraphicsImpl impl, int x, int y, int radius) {
        super(impl);
        this.x = x;
        this.y = y;
        this.radius = radius;
    }

    public void draw() {
        impl.drawCircle(x, y, radius);
    }
}

// 矩形类
class Rectangle extends Graphics {
    private int x1, y1, x2, y2;

    public Rectangle(GraphicsImpl impl, int x1, int y1, int x2, int y2) {
        super(impl);
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }

    public void draw() {
        impl.drawRectangle(x1, y1, x2, y2);
    }
}


public class BridgeDemo {
    // 客户端使用抽象图形类来绘制图形
    public static void main(String[] args) {
        GraphicsImpl windowsImpl = new WindowsGraphicsImpl();
        GraphicsImpl linuxImpl = new LinuxGraphicsImpl();

        Graphics circle1 = new Circle(windowsImpl, 10, 10, 5);
        Graphics circle2 = new Circle(linuxImpl, 20, 20, 10);
        Graphics rectangle1 = new Rectangle(windowsImpl, 5, 5, 10, 10);
        Graphics rectangle2 = new Rectangle(linuxImpl, 15, 15, 20, 20);

        circle1.draw();
        circle2.draw();
        rectangle1.draw();
        rectangle2.draw();
    }
}
