package com.zegoto.design.design.struct.composite;

import java.util.ArrayList;
import java.util.List;

// 抽象组件
interface Component {
    void show();
}

// 叶子节点，表示员工
class Employee implements Component {
    private String name;

    public Employee(String name) {
        this.name = name;
    }

    public void show() {
        System.out.println("Employee: " + name);
    }
}

// 组合节点，表示部门
class Department implements Component {
    private String name;
    private List<Component> children;

    public Department(String name) {
        this.name = name;
        this.children = new ArrayList<>();
    }

    public void add(Component component) {
        children.add(component);
    }

    public void remove(Component component) {
        children.remove(component);
    }

    public void show() {
        System.out.println("Department: " + name);
        for (Component component : children) {
            component.show();
        }
    }
}



public class CompositeDemo {

    // 客户端使用组件接口来操作组织机构
    public static void main(String[] args) {
        Component employee1 = new Employee("Tom");
        Component employee2 = new Employee("Jerry");
        Component employee3 = new Employee("Bob");

        Component department1 = new Department("Development");
        ((Department) department1).add(employee1);
        ((Department) department1).add(employee2);

        Component department2 = new Department("Marketing");
        ((Department) department2).add(employee3);

        Component organization = new Department("Organization");
        ((Department) organization).add(department1);
        ((Department) organization).add(department2);

        organization.show();
    }
}
