package com.zegoto.design.design.struct.decorator;

// 抽象组件
interface Component {
    void operation();
}

// 具体组件
class ConcreteComponent implements Component {
    public void operation() {
        System.out.println("ConcreteComponent operation");
    }
}

// 抽象装饰者
abstract class Decorator implements Component {
    protected Component component;

    Decorator(Component component) {
        this.component = component;
    }

    public void operation() {
        component.operation();
    }
}

// 具体装饰者
class ConcreteDecoratorA extends Decorator {
    ConcreteDecoratorA(Component component) {
        super(component);
    }

    public void operation() {
        super.operation();
        System.out.println("ConcreteDecoratorA operation");
    }
}

// 具体装饰者
class ConcreteDecoratorB extends Decorator{
        ConcreteDecoratorB(Component component){
            super(component);
        }

        public void operation(){
            super.operation();
            System.out.println("ConcreteDecoratorB operation");
        }
}

// 客户端
public class DecoratorDemo {
    public static void main(String[] args) {
        Component component = new ConcreteComponent();
        component = new ConcreteDecoratorA(component);
        component = new ConcreteDecoratorB(component);
        component.operation();
    }
}
