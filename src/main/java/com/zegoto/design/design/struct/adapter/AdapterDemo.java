package com.zegoto.design.design.struct.adapter;

// 目标接口
interface Target {
    void request();
}

// 源接口
class Adaptee {
    void specificRequest() {
        System.out.println("Adaptee specific request");
    }
}

// 适配器
class Adapter implements Target {
    private Adaptee adaptee;

    Adapter(Adaptee adaptee) {
        this.adaptee = adaptee;
    }

    public void request() {
        adaptee.specificRequest();
    }
}

// 客户端
public class AdapterDemo {
    public static void main(String[] args) {
        Adaptee adaptee = new Adaptee();
        Target adapter = new Adapter(adaptee);
        adapter.request();
    }
}
