package com.zegoto.design.design.struct.proxy;

// 抽象主题
interface Subject {
    void request();
}

// 真实主题
class RealSubject implements Subject {
    public void request() {
        System.out.println("RealSubject request");
    }
}

// 代理
class Proxy implements Subject {
    private RealSubject realSubject;

    public Proxy() {
        realSubject = new RealSubject();
    }

    public void request() {
        System.out.println("Proxy preRequest");
        realSubject.request();
        System.out.println("Proxy postRequest");
    }
}

// 客户端
public class ProxyDemo {
    public static void main(String[] args) {
        Subject proxy = new Proxy();
        proxy.request();
    }
}
