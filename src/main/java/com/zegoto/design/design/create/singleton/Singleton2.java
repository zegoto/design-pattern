package com.zegoto.design.design.create.singleton;

public class Singleton2 {

    private Singleton2() {}

    private static volatile Singleton2 instance = null;

    public static Singleton2 getInstance() {
        if (instance == null) {
            // 加锁
            synchronized (Singleton2.class) {
                // 这一次判断也是必须的，不然会有并发问题
                if (instance == null) {
                    instance = new Singleton2();
                }
            }
        }
        return instance;
    }

}
