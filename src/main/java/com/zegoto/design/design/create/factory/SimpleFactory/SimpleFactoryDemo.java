package com.zegoto.design.design.create.factory.SimpleFactory;
interface Product {
    void operation();
}

class ConcreteProduct1 implements Product {
    public void operation() {
        System.out.println("ConcreteProduct1.operation");
    }
}

class ConcreteProduct2 implements Product {
    public void operation() {
        System.out.println("ConcreteProduct2.operation");
    }
}

class Factory {
    public static Product createProduct(String type) {
        if (type.equals("product1")) {
            return new ConcreteProduct1();
        } else if (type.equals("product2")) {
            return new ConcreteProduct2();
        } else {
            throw new IllegalArgumentException("Invalid producttype: " + type);
        }
    }
}

public class SimpleFactoryDemo {
    public static void main(String[] args) {
        Product product1 = Factory.createProduct("product1");
        product1.operation();

        Product product2 = Factory.createProduct("product2");
        product2.operation();
    }
}