package com.zegoto.design.design.create.factory.factoryMethod;
interface Product {
    void operation();
}

class ConcreteProduct1 implements Product {
    public void operation() {
        System.out.println("ConcreteProduct1.operation");
    }
}

class ConcreteProduct2 implements Product {
    public void operation() {
        System.out.println("ConcreteProduct2.operation");
    }
}

interface Factory {
    Product createProduct();
}

class ConcreteFactory1 implements Factory {
    public Product createProduct() {
        return new ConcreteProduct1();
    }
}

class ConcreteFactory2 implements Factory {
    public Product createProduct() {
        return new ConcreteProduct2();
    }
}

public class FactoryMethodDemo {
    public static void main(String[] args) {
        Factory factory1 = new ConcreteFactory1();
        Product product1 = factory1.createProduct();
        product1.operation();

        Factory factory2 = new ConcreteFactory2();
        Product product2 = factory2.createProduct();
        product2.operation();
    }
}