package com.zegoto.design.design.create.factory.AbstractFactory;

interface AbstractFactory {
    ProductA createProductA();
    ProductB createProductB();
}

class ConcreteFactory1 implements AbstractFactory {
    public ProductA createProductA() {
        return new ConcreteProductA1();
    }

    public ProductB createProductB() {
        return new ConcreteProductB1();
    }
}

class ConcreteFactory2 implements AbstractFactory {
    public ProductA createProductA() {
        return new ConcreteProductA2();
    }

    public ProductB createProductB() {
        return new ConcreteProductB2();
    }
}

interface ProductA {
    void operationA();
}

interface ProductB {
    void operationB();
}

class ConcreteProductA1 implements ProductA {
    public void operationA() {
        System.out.println("ConcreteProductA1.operationA");
    }
}

class ConcreteProductA2 implements ProductA {
    public void operationA() {
        System.out.println("ConcreteProductA2.operationA");
    }
}

class ConcreteProductB1 implements ProductB {
    public void operationB() {
        System.out.println("ConcreteProductB1.operationB");
    }
}

class ConcreteProductB2 implements ProductB {
    public void operationB() {
        System.out.println("ConcreteProductB2.operationB");
    }
}

public class AbstractFactoryDemo {
    public static void main(String[] args) {
        AbstractFactory factory1 = new ConcreteFactory1();
        ProductA productA1 = factory1.createProductA();
        productA1.operationA();
        ProductB productB1 = factory1.createProductB();
        productB1.operationB();

        AbstractFactory factory2 = new ConcreteFactory2();
        ProductA productA2 = factory2.createProductA();
        productA2.operationA();
        ProductB productB2 = factory2.createProductB();
        productB2.operationB();
    }
}