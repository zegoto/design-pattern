package com.zegoto.design.design.create.builder;
class Product {
    private String part1;
    private String part2;

    public void setPart1(String part1) {
        this.part1 = part1;
    }

    public void setPart2(String part2) {
        this.part2 = part2;
    }

    public void print() {
        System.out.println("part1 = " + part1 + ", part2 = " + part2);
    }
}

class ProductBuilder{

    private Product product;

    private String part1;

    private String part2;

    public ProductBuilder buildPart1(String part1) {
        this.part1 = part1;
        return this;
    }

    public ProductBuilder buildPart2(String part2) {
        this.part2 = part2;
        return this;
    }

    public Product build() {
        product = new Product();
        product.setPart1(this.part1);
        product.setPart2(this.part2);
        return product;
    }
}

public class BuilderDemo {
    public static void main(String[] args) {
        Product product = new ProductBuilder()
                .buildPart1("part1")
                .buildPart2("part2")
                .build();
        product.print();
    }
}
